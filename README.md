CubeUnivers_CPP
============

Clone de CubeWorld, réimplémentation de mon projet CubeUnivers mais en C++ et OpenGL 4.x


<br>

Instructions de compilation :
=====
Requierements:
-----
**Librairie requises :**
(Version testées, les versions antérieurs/suivantes sont susceptibles de marcher)
* SDL2 2.0.12
* GLEW 2.2.0
* GLM 0.9.9.8

**Outils requis:**
* CMake 3.X
  
Instruction :
----
Dans le dossier racine du dépôt, executer :
```sh
cmake . && cmake --build .
```


<br>

TODO :
====
* Performance:  
  * ~~Optimisation gestion des blocs (Conso RAM élevée)~~
  - Multi-threading dans la génération des chunks
* Fonctionnalitées:
  * Système de configuration
  * Système de logging
  * Système de sauvegarde des mondes
  * Génération : ajouter + de biomes, décorations (i.e Végétations, Lacs) et structures (Donjon et Villages)
  * Tout le Gameplay (Joueur, monstres, etc...)
* Rendu:
  * ~~Ombres~~
  * Ciel (Soleil/Lune + atmospheric scattering avec couleur)
  * Lumière dynamique (lanterne)
 

Contributeurs
=====
Antoine 'Theucalyptus' Lebeault