#ifndef JOUEUR_H
#define JOUEUR_H

#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>
#include <glm/ext/matrix_transform.hpp>

#include "Autres/Enumerations.hpp"

using namespace glm;

class Joueur {

    public:
        Joueur();
        virtual ~Joueur();

        void Deplacer(DIRECTION direction);
        vec3 getPosition();
        void Rendre();

        void Orienter(vec3 orientation);

    private:
        vec3 m_position;
        vec3 m_orientation;
    
};

#endif // JOUEUR_H
