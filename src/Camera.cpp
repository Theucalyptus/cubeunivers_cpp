#include  "Camera.hpp"


#include <iostream>

Camera::Camera(Joueur& joueur) : m_joueur(joueur) {

}

Camera::~Camera() {

}

void Camera::Orienter(int deltaX_Relatif, int deltaY_Relatif) {
    
    //Inversement des coordonnées car sens anti-horaire 
    float xRel = (float) deltaX_Relatif/10;
    float yRel = (float) -deltaY_Relatif/10;

    // Emp
    if(m_phi+yRel <= -90) {
        m_phi = -89.9;
        yRel = 0;
    }
    else if(m_phi+yRel >= 90) {
        m_phi = 89.9;
        yRel = 0;
    }
    else 
        m_phi += yRel;

    m_theta += xRel;

    // 'Normalization' des coordonnées de la caméra
    if(m_theta <= -360)
        m_theta += 360;
    else if (m_theta >= 360)
        m_theta -= 360;


    m_orientation = normalize(vec3(  
                cos(radians(m_phi))*cos(radians(m_theta)), 
                sin(radians(m_phi)), 
                cos(radians(m_phi))*sin(radians(m_theta))));


}

void Camera::Cibler(vec3 point_a_cibler) {
    
    if(point_a_cibler == vec3(0.0,0.0,0.0)) {
        m_cible = m_joueur.getPosition() + m_orientation;
    }
    else {
        m_cible = point_a_cibler;
        std::cout << "Camera: Cibler(vec3) ne calcul pas les angles, dont dès que la souris bouge elle revient à son ancienne orientation." << std::endl;
    }

}

mat4 Camera::getViewMatrice() {
    return glm::lookAt(m_joueur.getPosition(), m_cible, VECTEUR_VERTICAL);
}


vec3 Camera::getOrientation() {
    return m_orientation;
}