#include  "Joueur.hpp"

Joueur::Joueur() : m_position(vec3(0.0,85.0,0.0)), m_orientation(vec3(1.0,0.0,0.0))  {

}

Joueur::~Joueur() {

}


void Joueur::Deplacer(DIRECTION direction) {
    vec3 joueurX  =  normalize(cross(VECTEUR_VERTICAL, m_orientation));  //représente l'axe X de la caméra prenant en compte la rotation de la camera

    switch (direction) {
        case DIRECTION::AVANT:
            m_position += m_orientation * 0.3f;
            break;
        
        case DIRECTION::ARRIERE:
            m_position -= m_orientation * 0.3f;
            break;
        
        case DIRECTION::GAUCHE:
            m_position -= joueurX * 0.3f;
            break;
        
        case DIRECTION::DROITE:
            m_position += joueurX * 0.3f;
            break;
        case DIRECTION::HAUT:
            m_position.y += 0.3;
            break;
        case DIRECTION::BAS:
            m_position.y -= 0.3;
            break;
        default:
            break;
    }
}

vec3 Joueur::getPosition() {
    return m_position;
}


void Joueur::Orienter(vec3 orientation){
    m_orientation = orientation;
}

void Joueur::Rendre() {
    /// @TODO
}