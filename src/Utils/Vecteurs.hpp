#ifndef VECTEURS_H
#define VECTEURS_H

template <typename T>
class Vecteur2D {
public:
    Vecteur2D(size_t tailleX, size_t tailleY) : tailleX(tailleX), tailleY(tailleY), donnees(tailleX*tailleY){
    }

    T & operator()(size_t x, size_t y) {
        return donnees[x*tailleY + y];
    }

    T const & operator()(size_t x, size_t y) const {
        return donnees[x*tailleY + y];
    }

    private:
        size_t tailleX, tailleY;
        std::vector<T> donnees;
};

template <typename T>
class Vecteur3D {
public:
    Vecteur3D(size_t tailleX, size_t tailleY, size_t tailleZ) : tailleX(tailleX), tailleY(tailleY), tailleZ(tailleZ), donnees(tailleX*tailleY*tailleZ)
    {}

    T & operator()(size_t x, size_t y, size_t z) {
        return donnees[x*tailleY*tailleZ + y*tailleZ + z];
    }

    T const & operator()(size_t x, size_t y, size_t z) const {
        return donnees[x*tailleY*tailleZ + y*tailleZ + z];
    }


private:
    size_t tailleX,tailleY,tailleZ;
    std::vector<T> donnees;
};

#endif //VECTEURS_H