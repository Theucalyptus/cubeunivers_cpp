#ifndef SDLMANAGER_H
#define SDLMANAGER_H

#include <SDL2/SDL.h>
#include <GL/glew.h>

#define RES_X 1920
#define RES_Y 1080


    bool InitialiserSDL();
    bool InitialiserGLEW();
    
    SDL_Window* CreerFenetre(char nom[], int posX=SDL_WINDOWPOS_UNDEFINED,int posY=SDL_WINDOWPOS_UNDEFINED, int resX=RES_X, int resY=RES_Y);
    bool CreerContextGL(SDL_Window* fenetre);
    void QuitterSDL(SDL_Window* fenetre);

#endif // SDLMANAGER_H
