#include  "SDLManager.hpp"

#include <iostream>

using namespace std;

bool InitialiserSDL() {
    
    
    if (SDL_Init(SDL_INIT_VIDEO) != 0) {
        cout << "SDL_Init Error: " << SDL_GetError() << endl;
        return false;
    }  

    SDL_version ver;
    SDL_GetVersion(&ver);
    cout << "Démarre avec SDL " << unsigned(ver.major) << "." << unsigned(ver.minor) << "." << unsigned(ver.patch) << endl;        SDL_VERSION(&ver);
    cout << "Compilé contre SDL " << unsigned(ver.major) << "." << unsigned(ver.minor) << "." << unsigned(ver.patch) << endl;

    
    //Utilisation de OpenGL 4.2 Core
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
    SDL_GL_SetAttribute(SDL_GL_FRAMEBUFFER_SRGB_CAPABLE, 1);

     SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
    SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 8);


    SDL_CaptureMouse(SDL_FALSE);
    SDL_SetRelativeMouseMode(SDL_TRUE);


    return true;
}

bool InitialiserGLEW() {
             
    GLenum error = glewInit();
    if (error != GLEW_OK) {
        cout << "glewInit Error : " << glewGetErrorString(error) << endl;
        return false;
    }

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_MULTISAMPLE);
    glEnable(GL_FRAMEBUFFER_SRGB); 
    glEnable(GL_CULL_FACE);

    return true;
}

SDL_Window* CreerFenetre(char nom[], int posX,int posY, int resX, int resY) {
    
    // Creer la fenètre 
    SDL_Window* fenetre = SDL_CreateWindow(nom, posX,  posY, resX, resY, SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL);
    if (fenetre == NULL) {
        cout << "SDL_CreateWindow Error: " << SDL_GetError() << endl;
    }
    return fenetre;


}

bool CreerContextGL(SDL_Window* fenetre) {
    
    
    // Création du contexte OpenGL 4:

    cout << "Création du contexte OpenGL." << endl;

    if(!SDL_GL_CreateContext(fenetre)) {
        cout << "SDL_GL_CreateContext Error: " << SDL_GetError() << endl;
        return false;
    }
 
    const GLubyte* gl_version;
    gl_version = glGetString(GL_VERSION);
    cout << "Utilise OpenGL " << gl_version << endl;

    if(SDL_GL_SetSwapInterval(0) != 0) {
        cout << "Erreur à la désactivation de la Vsync." << endl;
    }

    return true;
}

void QuitterSDL(SDL_Window* fenetre) {
    // Fonctions pour quitter proprement la SDL    
    SDL_GL_DeleteContext(SDL_GL_GetCurrentContext()); // Cette solution ne marche qu'avec un seul contexte OpenGL
    SDL_DestroyWindow(fenetre);
    SDL_Quit();
}
