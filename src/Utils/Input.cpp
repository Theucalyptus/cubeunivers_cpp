#include  "Input.hpp"

#include <iostream>


Input::Input() : m_quitter(false), m_vsync(false), sourisX(0), sourisY(0), sourisX_Relatif(0) ,sourisY_Relatif(0) {
    //Constucteur

    for(int i=0;i<SDL_NUM_SCANCODES;i++) {
        keyboard_status[i] = false;
    }
}

Input::~Input() {
    // Deconstructeur
}

void Input::SetInputMethode() {

}

void Input::UpdateInput(Joueur &joueur, Camera &camera) {

    //Gestion des évenements SDL
    while (SDL_PollEvent(&m_evenements))
    {
        switch (m_evenements.type)
        {
            case SDL_KEYDOWN:
                keyboard_status[m_evenements.key.keysym.scancode] = true;
                break;
            case SDL_KEYUP:
                keyboard_status[m_evenements.key.keysym.scancode] = false;
                break;
            case SDL_MOUSEMOTION:
                sourisX_Relatif = m_evenements.motion.xrel;
                sourisY_Relatif = m_evenements.motion.yrel;
                sourisX = m_evenements.motion.x;
                sourisY = m_evenements.motion.y;
                camera.Orienter(sourisX_Relatif ,sourisY_Relatif);
                joueur.Orienter(camera.getOrientation());
                break;
            default:
                break;
        }
    }

    //update for keyboard inputs
    if (keyboard_status[SDL_SCANCODE_ESCAPE] )
        m_quitter = true;
    if (keyboard_status[SDL_SCANCODE_W]) {
        joueur.Deplacer(DIRECTION::AVANT); 
    }
    if (keyboard_status[SDL_SCANCODE_S]) {
        joueur.Deplacer(DIRECTION::ARRIERE); 
    }
    if (keyboard_status[SDL_SCANCODE_D]) {
        joueur.Deplacer(DIRECTION::GAUCHE); 
    }
    if (keyboard_status[SDL_SCANCODE_A]) {
        joueur.Deplacer(DIRECTION::DROITE); 
    }
    if (keyboard_status[SDL_SCANCODE_Q]) {
        joueur.Deplacer(DIRECTION::HAUT); 
    }
    if (keyboard_status[SDL_SCANCODE_E]) {
        joueur.Deplacer(DIRECTION::BAS); 
    }
    if (keyboard_status[SDL_SCANCODE_P]) {
        keyboard_status[SDL_SCANCODE_P] = false; // consume the input to not change vsync every frame
        m_vsync = !m_vsync;
        SDL_GL_SetSwapInterval(m_vsync);
    }

}

bool Input::Terminer() {
    return m_quitter;
}