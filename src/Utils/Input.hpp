#ifndef INPUT_H
#define INPUT_H

#include <SDL2/SDL.h>


#include "../Camera.hpp"

using namespace std;

class Input {
    
    public:
        //Constructeur et Deconstructeur
        Input();
        virtual ~Input();
        void SetInputMethode();
        void UpdateInput(Joueur &joueur, Camera &camera);
        bool Terminer();
    
    private:
        SDL_Event m_evenements;
        bool m_quitter;
        bool m_vsync;

        int sourisX;
        int sourisY;
        int sourisX_Relatif;
        int sourisY_Relatif;

        bool keyboard_status[SDL_NUM_SCANCODES];

};

#endif // INPUT_H
