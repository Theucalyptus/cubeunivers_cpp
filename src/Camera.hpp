#ifndef CAMERA_H
#define CAMERA_H

#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>
#include <glm/ext/matrix_transform.hpp>

#include "Autres/Enumerations.hpp"
#include "Joueur.hpp"


using namespace glm;


class Camera {

    public:
        Camera() = default;
        Camera(Joueur& joueur);
        virtual ~Camera();

        void Deplacer(DIRECTION direction);
        void Orienter(int deltaX_Relatif, int deltaY_Relatif);
        void Cibler(vec3 point_a_cibler=vec3(0.0f,0.0f,0.0f));
        mat4 getViewMatrice();
        vec3 getPosition();
        vec3 getOrientation();


    private:
        Joueur& m_joueur;
        vec3 m_orientation;
        vec3 m_cible;

        int m_deltaX;
        int m_deltaY;
        float m_phi;
        float m_theta;



        
};

#endif // CAMERA_H
