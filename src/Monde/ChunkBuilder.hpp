#ifndef ChunkBuilder_H
#define ChunkBuilder_H


#include <mutex>

#include "../..//lib/FastNoise/FastNoise.h"

#include "Chunk.hpp"
#include "Bloc/Bloc.hpp"
#include "Structures/Structures.hpp"
#include "../Autres/Enumerations.hpp"

class ChunkBuilder {

    public:    
        static Chunk GenererChunk(int seed, int coordX, int coordY);

    private:
        static int getHauteur(float noiseLevel);

};

#endif // ChunkBuilder_H
