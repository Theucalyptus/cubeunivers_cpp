#include  "Structures.hpp"


Structure::Structure(STRUCTURES structure) : tailleX(0), tailleY(0), tailleZ(0), m_blocs(tailleX, tailleY, tailleZ) {
    // Constructeur
    
    std::ifstream input_file(fichiers_structure[as_integer(structure)], std::ifstream::binary);
    if(input_file) {
        Json::Value data;
        input_file >> data;

        // Chargement de la taille de la structure
        const Json::Value dimensions = data["dimensions"];
        tailleX = dimensions.get("tailleX", 0).asInt();
        tailleY = dimensions.get("tailleY", 0).asInt();
        tailleZ = dimensions.get("tailleZ", 0).asInt();

        offsetX = tailleX/2;
        offsetZ = tailleZ/2;

        m_blocs = Vecteur3D<StructBloc>(tailleX, tailleY, tailleZ);

        for(int y=0;y<tailleY; y++) {
            Json::Value couche = data["blocs"]["couche" + std::to_string(y)];
            for(int x=0;x<tailleX; x++) {
                
                Json::Value ligne = couche["ligne" + std::to_string(x)];  
                for(int z=0;z<tailleZ; z++) {
                    
                    Json::Value bloc = ligne["bloc" + std::to_string(z)];
                    m_blocs(x,y,z).type = static_cast<BLOC_TYPES>(bloc.get("type", 0).asUInt());

                    if(m_blocs(x,y,z).type == BLOC_TYPES::PLEIN) {
                        // récupération de la couleur
                        Json::Value couleur = bloc["couleur"];
                        m_blocs(x, y, z).couleur1 = couleur.get("couleur1", 0).asUInt();
                        m_blocs(x, y, z).couleur2 = couleur.get("couleur2", 0).asUInt();
                        m_blocs(x, y, z).couleur_variation = couleur.get("variation", 0).asFloat();
                    }
                }
            }
        }
    }
    else {
        std::cout << "Erreur: impossible de charger le fichier structure "<< fichiers_structure[as_integer(structure)] << std::endl;
    }
    
}

Structure::~Structure() {
    // Deconstructeur
}