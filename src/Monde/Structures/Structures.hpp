#ifndef STRUCTURES_H
#define STRUCTURES_H

#include <json/json.h>
#include <fstream>
#include <iostream>

#include "../../Utils/Vecteurs.hpp"
#include "../Bloc/Bloc.hpp"
#include "../../Autres/Enumerations.hpp"

struct StructBloc {
    
    BLOC_TYPES type;
    unsigned int couleur1;
    unsigned int couleur2;
    float couleur_variation;

};

struct Structure {
    
    Structure(STRUCTURES structure);
    ~Structure();

    int tailleX, tailleY, tailleZ;
    int offsetX, offsetZ;
    Vecteur3D<StructBloc> m_blocs;
};

#endif //STRUCTURES_H