#include "Chunk.hpp"

#include "Monde.hpp"

using namespace std;

Chunk::Chunk() : m_blocs(CHUNK_SIZE, CHUNK_HEIGHT, CHUNK_SIZE) {
    //std::cout << "Chunk default allocator called !" << std::endl;

}

Chunk::Chunk(int coordX, int coordY) : m_coordX(coordX), m_coordY(coordY), m_nbrObjet(0), m_vaoID(0), m_eboID(0), m_vboID(0),  m_blocs(CHUNK_SIZE, CHUNK_HEIGHT, CHUNK_SIZE)
{
    //std::cout << "Chunk Custom allocator called !" << std::endl;
    //Constructeur
    m_model = glm::translate(glm::mat4(1.0), glm::vec3(m_coordX * CHUNK_SIZE, 0.0, m_coordY * CHUNK_SIZE));
    x_offset = m_coordX * CHUNK_SIZE;
    z_offset = m_coordY * CHUNK_SIZE;

}

Chunk::~Chunk()
{
    //std::cout << "Chunk Desctructeur called !" << std::endl;
    
    if (glIsBuffer(m_vboID))
    {
        glDeleteBuffers(1, &m_vboID);
    }
    if (glIsVertexArray(m_vaoID))
    {
        glDeleteVertexArrays(1, &m_vaoID);
    }
    if (glIsBuffer(m_eboID))
    {
        glDeleteVertexArrays(1, &m_eboID);
    }
}

void Chunk::GenererMesh()
{

    cout << "Génération du mesh de " << m_coordX <<  " " << m_coordY << endl;

    vector<GLfloat> vbo_data;
    vector<GLuint> ebo_data;


    for (int x = 0; x < CHUNK_SIZE; ++x){
        for (int y = 0; y < CHUNK_HEIGHT; ++y){
            for (int z = 0; z < CHUNK_SIZE; ++z){
                glm::vec3 coord = glm::vec3(x, y, z);
                if (isSolidBlock(x, y, z)){   
                    if (!isSolidBlock(x, y-1, z)){
                        _ajouterFace2Mesh(DIRECTION::BAS, vbo_data, ebo_data, coord);
                    }
                    if (!isSolidBlock(x, y+1, z)){
                        _ajouterFace2Mesh(DIRECTION::HAUT, vbo_data, ebo_data, coord);
                    }        
                    if (!isSolidBlock(x-1,y,z)) {
                        _ajouterFace2Mesh(DIRECTION::GAUCHE, vbo_data, ebo_data, coord);
                    }
                    if (!isSolidBlock(x+1,y,z)) {
                        _ajouterFace2Mesh(DIRECTION::DROITE, vbo_data, ebo_data, coord);
                    }
                    if (!isSolidBlock(x,y,z-1)) {
                        _ajouterFace2Mesh(DIRECTION::AVANT, vbo_data, ebo_data, coord);
                    }
                    if (!isSolidBlock(x,y,z+1)) {
                        _ajouterFace2Mesh(DIRECTION::ARRIERE, vbo_data, ebo_data, coord);
                    }

                }
            }
        }
    }    

    m_nbrObjet = ebo_data.size();
    GenererVBO(vbo_data);
    GenererEBO(ebo_data);
    GenererVAO();
}

void Chunk::_ajouterFace2Mesh(DIRECTION face, vector<GLfloat> &vbo_data, vector<GLuint> &ebo_data, glm::vec3 coord)
{
    // Paterne ds points d'une face de cube à 4 vertices d'entrées:
    //int nbr_elem = ebo_data.size();
    int nbr_elem =vbo_data.size()/10;
    ebo_data.push_back(nbr_elem);
    ebo_data.push_back(nbr_elem+2);
    ebo_data.push_back(nbr_elem+1);
    ebo_data.push_back(nbr_elem+1);
    ebo_data.push_back(nbr_elem+2);
    ebo_data.push_back(nbr_elem+3);

    Bloc* target = getBloc(coord.x, coord.y, coord.z);   

    BlocPlein* bPtr = BlocPlein::fromBlocPtr(target);
    if(bPtr) {
        int index=0;
        for (auto &a : bPtr->getVerticesFace(face))
        {
            a += coord; //vertice
            vbo_data.push_back(a.x);
            vbo_data.push_back(a.y);
            vbo_data.push_back(a.z);

            //couleur
            Couleur c = bPtr->getCouleurForMesh(face, index);
            vbo_data.push_back(c.x);
            vbo_data.push_back(c.y);
            vbo_data.push_back(c.z);

            //normale
            Bloc* b = getBloc(coord.x, coord.y, coord.z);
            BlocPlein bloc = *BlocPlein::fromBlocPtr(b); // PEUT FAIL MAIS NORMALEMENT NON
            auto v = bloc.getNormalesFace(face);
            vbo_data.push_back(v.x);
            vbo_data.push_back(v.y);
            vbo_data.push_back(v.z);

            //lumières 
            vbo_data.push_back((GLfloat)0.1f);
            index++;
        }
    } else {
        std::cout << "Chunk::_ajouterFace2Mesh dynamic cast failed ! " << std::endl;
    }

}

// glm::vec3* Chunk::_lisserCouleur(glm::vec3 coord)
// {
//
//     auto calcMoyenne = [&](vector<glm::vec3> temp_couleurs) {
//         glm::vec3 temp_couleur = glm::vec3(0);
//         for (auto &a : temp_couleurs)
//         {
//             temp_couleur += a;
//         }
//         temp_couleur.x = temp_couleur.x / (float)(temp_couleurs.size());
//         temp_couleur.y = temp_couleur.y / (float)(temp_couleurs.size());
//         temp_couleur.z = temp_couleur.z / (float)(temp_couleurs.size());Z
//         return temp_couleur;
//     };
//
//     static glm::vec3 sortie[4];
//
//     // Calcul la couleur moyenne des quatres blocs autour du coin inférieur gauche
//     vector<glm::vec3> temp_couleurs;
//     if (m_monde->getBloc((coord.x + x_offset - 1), coord.y, (coord.z + z_offset))->getType() == BLOC_TYPES::PLEIN)
//         temp_couleurs.push_back(m_monde->getBloc((coord.x + x_offset - 1), coord.y, (coord.z + z_offset))->getBlocClass()->getCouleurBloc());
//     if (m_monde->getBloc((coord.x + x_offset - 1), coord.y, (coord.z + z_offset - 1))->getType() == BLOC_TYPES::PLEIN)
//         temp_couleurs.push_back(m_monde->getBloc((coord.x + x_offset - 1), coord.y, (coord.z + z_offset - 1))->getBlocClass()->getCouleurBloc());
//     if (m_monde->getBloc((coord.x + x_offset), coord.y, (coord.z + z_offset - 1))->getType() == BLOC_TYPES::PLEIN)
//         temp_couleurs.push_back(m_monde->getBloc((coord.x + x_offset), coord.y, (coord.z + z_offset - 1))->getBlocClass()->getCouleurBloc());
//     temp_couleurs.push_back(m_blocs(coord.x, coord.y, coord.z).getBlocClass()->getCouleurBloc());
//     sortie[0] = calcMoyenne(temp_couleurs);
//
//     // Calcul la couleur moyenne des quatres blocs autour du coin inférieur droit
//     temp_couleurs.clear();
//     if (m_monde->getBloc((coord.x + x_offset), coord.y, (coord.z + z_offset - 1))->getType() == BLOC_TYPES::PLEIN)
//         temp_couleurs.push_back(m_monde->getBloc((coord.x + x_offset), coord.y, (coord.z + z_offset - 1))->getBlocClass()->getCouleurBloc());
//     if (m_monde->getBloc((coord.x + x_offset + 1), coord.y, (coord.z + z_offset - 1))->getType() == BLOC_TYPES::PLEIN)
//         temp_couleurs.push_back(m_monde->getBloc((coord.x + x_offset + 1), coord.y, (coord.z + z_offset - 1))->getBlocClass()->getCouleurBloc());
//     if (m_monde->getBloc((coord.x + x_offset + 1), coord.y, (coord.z + z_offset))->getType() == BLOC_TYPES::PLEIN)
//         temp_couleurs.push_back(m_monde->getBloc((coord.x + x_offset + 1), coord.y, (coord.z + z_offset))->getBlocClass()->getCouleurBloc());
//
//     temp_couleurs.push_back(m_blocs(coord.x, coord.y, coord.z).getBlocClass()->getCouleurBloc());
//     sortie[1] = calcMoyenne(temp_couleurs);
//
//     // Calcul la couleur moyenne des quatres blocs autour du coin supérieur gauche
//     temp_couleurs.clear();
//     if (m_monde->getBloc((coord.x + x_offset - 1), coord.y, (coord.z + z_offset))->getType() == BLOC_TYPES::PLEIN)
//         temp_couleurs.push_back(m_monde->getBloc((coord.x + x_offset - 1), coord.y, (coord.z + z_offset))->getBlocClass()->getCouleurBloc());
//     if (m_monde->getBloc((coord.x + x_offset - 1), coord.y, (coord.z + z_offset + 1))->getType() == BLOC_TYPES::PLEIN)
//         temp_couleurs.push_back(m_monde->getBloc((coord.x + x_offset - 1), coord.y, (coord.z + z_offset + 1))->getBlocClass()->getCouleurBloc());
//     if (m_monde->getBloc((coord.x + x_offset), coord.y, (coord.z + z_offset + 1))->getType() == BLOC_TYPES::PLEIN)
//         temp_couleurs.push_back(m_monde->getBloc((coord.x + x_offset), coord.y, (coord.z + z_offset + 1))->getBlocClass()->getCouleurBloc());
//     temp_couleurs.push_back(m_blocs(coord.x, coord.y, coord.z).getBlocClass()->getCouleurBloc());
//     sortie[2] = calcMoyenne(temp_couleurs);
//
//     // Calcul la couleur moyenne des quatres blocs autour du coin supérieur droit
//     temp_couleurs.clear();
//     if (m_monde->getBloc((coord.x + x_offset), coord.y, (coord.z + z_offset + 1))->getType() == BLOC_TYPES::PLEIN)
//         temp_couleurs.push_back(m_monde->getBloc((coord.x + x_offset), coord.y, (coord.z + z_offset + 1))->getBlocClass()->getCouleurBloc());
//     if (m_monde->getBloc((coord.x + x_offset + 1), coord.y, (coord.z + z_offset + 1))->getType() == BLOC_TYPES::PLEIN)
//         temp_couleurs.push_back(m_monde->getBloc((coord.x + x_offset + 1), coord.y, (coord.z + z_offset) + 1)->getBlocClass()->getCouleurBloc());
//     if (m_monde->getBloc((coord.x + x_offset + 1), coord.y, (coord.z + z_offset))->getType() == BLOC_TYPES::PLEIN)
//         temp_couleurs.push_back(m_monde->getBloc((coord.x + x_offset + 1), coord.y, (coord.z + z_offset))->getBlocClass()->getCouleurBloc());
//     temp_couleurs.push_back(m_blocs(coord.x, coord.y, coord.z).getBlocClass()->getCouleurBloc());
//     sortie[3] = calcMoyenne(temp_couleurs);
//
//     return sortie;
// }
//
// float* Chunk::_lisserLumiere(glm::vec3 coord) {
//
//     auto calcMoyenne = [&](vector<float> temp_niveaux) {
//         float temp_niveau = 0.0f;
//         for (auto &a : temp_niveaux)
//         {
//             temp_niveau += a;
//         }
//         temp_niveau = temp_niveau / (float)(temp_niveaux.size());
//         return temp_niveau;
//     };
//     vector<float> temp_niveaux;
//     static float sortie[4];
//             // Calcul la couleur moyenne des quatres blocs autour du coin inférieur gauche
//             if (m_monde->getBloc((coord.x + x_offset - 1), coord.y, (coord.z + z_offset))->getType() == BLOC_TYPES::PLEIN)
//                 temp_niveaux.push_back(m_monde->getBloc((coord.x + x_offset - 1), coord.y, (coord.z + z_offset))->getBlocClass()->getNiveauLumiere());
//             if (m_monde->getBloc((coord.x + x_offset - 1), coord.y, (coord.z + z_offset - 1))->getType() == BLOC_TYPES::PLEIN)
//                 temp_niveaux.push_back(m_monde->getBloc((coord.x + x_offset - 1), coord.y, (coord.z + z_offset - 1))->getBlocClass()->getNiveauLumiere());
//             if (m_monde->getBloc((coord.x + x_offset), coord.y, (coord.z + z_offset - 1))->getType() == BLOC_TYPES::PLEIN)
//                 temp_niveaux.push_back(m_monde->getBloc((coord.x + x_offset), coord.y, (coord.z + z_offset - 1))->getBlocClass()->getNiveauLumiere());
//             temp_niveaux.push_back(m_blocs(coord.x, coord.y, coord.z).getBlocClass()->getNiveauLumiere());
//             sortie[0] = calcMoyenne(temp_niveaux);
//             // Calcul la couleur moyenne des quatres blocs autour du coin inférieur droit
//             temp_niveaux.clear();
//             if (m_monde->getBloc((coord.x + x_offset), coord.y, (coord.z + z_offset - 1))->getType() == BLOC_TYPES::PLEIN)
//                 temp_niveaux.push_back(m_monde->getBloc((coord.x + x_offset), coord.y, (coord.z + z_offset - 1))->getBlocClass()->getNiveauLumiere());
//             if (m_monde->getBloc((coord.x + x_offset + 1), coord.y, (coord.z + z_offset - 1))->getType() == BLOC_TYPES::PLEIN)
//                 temp_niveaux.push_back(m_monde->getBloc((coord.x + x_offset + 1), coord.y, (coord.z + z_offset - 1))->getBlocClass()->getNiveauLumiere());
//             if (m_monde->getBloc((coord.x + x_offset + 1), coord.y, (coord.z + z_offset))->getType() == BLOC_TYPES::PLEIN)
//                 temp_niveaux.push_back(m_monde->getBloc((coord.x + x_offset + 1), coord.y, (coord.z + z_offset))->getBlocClass()->getNiveauLumiere());
//
//             temp_niveaux.push_back(m_blocs(coord.x, coord.y, coord.z).getBlocClass()->getNiveauLumiere());
//             sortie[1] = calcMoyenne(temp_niveaux);
//
//             // Calcul la couleur moyenne des quatres blocs autour du coin supérieur gauche
//             temp_niveaux.clear();
//             if (m_monde->getBloc((coord.x + x_offset - 1), coord.y, (coord.z + z_offset))->getType() == BLOC_TYPES::PLEIN)
//                 temp_niveaux.push_back(m_monde->getBloc((coord.x + x_offset - 1), coord.y, (coord.z + z_offset))->getBlocClass()->getNiveauLumiere());
//             if (m_monde->getBloc((coord.x + x_offset - 1), coord.y, (coord.z + z_offset + 1))->getType() == BLOC_TYPES::PLEIN)
//                 temp_niveaux.push_back(m_monde->getBloc((coord.x + x_offset - 1), coord.y, (coord.z + z_offset + 1))->getBlocClass()->getNiveauLumiere());
//             if (m_monde->getBloc((coord.x + x_offset), coord.y, (coord.z + z_offset + 1))->getType() == BLOC_TYPES::PLEIN)
//                 temp_niveaux.push_back(m_monde->getBloc((coord.x + x_offset), coord.y, (coord.z + z_offset + 1))->getBlocClass()->getNiveauLumiere());
//             temp_niveaux.push_back(m_blocs(coord.x, coord.y, coord.z).getBlocClass()->getNiveauLumiere());
//             sortie[2] = calcMoyenne(temp_niveaux);
//
//             // Calcul la couleur moyenne des quatres blocs autour du coin supérieur droit
//             temp_niveaux.clear();
//             if (m_monde->getBloc((coord.x + x_offset), coord.y, (coord.z + z_offset + 1))->getType() == BLOC_TYPES::PLEIN)
//                 temp_niveaux.push_back(m_monde->getBloc((coord.x + x_offset), coord.y, (coord.z + z_offset + 1))->getBlocClass()->getNiveauLumiere());
//             if (m_monde->getBloc((coord.x + x_offset + 1), coord.y, (coord.z + z_offset + 1))->getType() == BLOC_TYPES::PLEIN)
//                 temp_niveaux.push_back(m_monde->getBloc((coord.x + x_offset + 1), coord.y, (coord.z + z_offset) + 1)->getBlocClass()->getNiveauLumiere());
//             if (m_monde->getBloc((coord.x + x_offset + 1), coord.y, (coord.z + z_offset))->getType() == BLOC_TYPES::PLEIN)
//                 temp_niveaux.push_back(m_monde->getBloc((coord.x + x_offset + 1), coord.y, (coord.z + z_offset))->getBlocClass()->getNiveauLumiere());
//             temp_niveaux.push_back(m_blocs(coord.x, coord.y, coord.z).getBlocClass()->getNiveauLumiere());
//             sortie[3] = calcMoyenne(temp_niveaux);
//
//     return sortie;
// }


// Getters
glm::mat4 Chunk::getModel()
{
    return m_model;
}

Bloc* Chunk::getBloc(int coordX, int coordY, int coordZ)
{
    return m_blocs(coordX, coordY, coordZ);
}

void Chunk::setBlock(Bloc* bloc, int x, int y, int z) {
    if(m_blocs(x, y, z)) {
        delete m_blocs(x, y, z);
    }
    m_blocs(x, y, z) = bloc;
}


int Chunk::getCoordX() {
    return m_coordX;
}
int Chunk::getCoordY() {
    return m_coordY;
}

//Fonction pour l'affichage / en lien avec OpenGL
void Chunk::GenererVBO(vector<GLfloat> &data)
{
    if (glIsBuffer(m_vboID))
    {
        glDeleteBuffers(1, &m_vboID);
    }

    // Création du VBO
    glGenBuffers(1, &m_vboID);
    glBindBuffer(GL_ARRAY_BUFFER, m_vboID);
    glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * data.size(), data.data(), GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void Chunk::GenererEBO(vector<GLuint> &data) {

    if (glIsBuffer(m_eboID))
    {
        glDeleteBuffers(1, &m_eboID);
    }

    // Création de l'EBO
    glGenBuffers(1, &m_eboID);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_eboID);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint) * data.size(), data.data(), GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void Chunk::GenererVAO()
{

    if (glIsVertexArray(m_vaoID))
    {
        glDeleteVertexArrays(1, &m_vaoID);
    }

    // Création du VAO;
    glCreateVertexArrays(1, &m_vaoID);
    glBindVertexArray(m_vaoID);  
    glBindBuffer(GL_ARRAY_BUFFER, m_vboID);

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 10 * sizeof(GLfloat), (void *)0); //Vertices
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 10 * sizeof(GLfloat), (void *)(3 * sizeof(GLfloat))); //couleurs
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 10 * sizeof(GLfloat), (void*)(6 * sizeof(GLfloat))); //normales
    glEnableVertexAttribArray(2);
    glVertexAttribPointer(3, 1, GL_FLOAT, GL_FALSE, 10 * sizeof(GLfloat), (void*)(9 * sizeof(GLfloat))); // lumieres
    glEnableVertexAttribArray(3);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_eboID);


    glBindVertexArray(0);
}

void Chunk::Rendre()
{
    glBindVertexArray(m_vaoID);
    glDrawElements(GL_TRIANGLES, m_nbrObjet, GL_UNSIGNED_INT, 0);
    glBindVertexArray(0);
}


bool Chunk::isValidChunkCoord(int bX, int bY, int bZ) {
    return (bX >= 0 && bX < CHUNK_SIZE) && (bZ >= 0 && bZ < CHUNK_SIZE) && (bY >= 0 && bY < CHUNK_HEIGHT);
}

bool Chunk::isSolidBlock(int x, int y, int z) {
    
    if(isValidChunkCoord(x, y, z) && getBloc(x, y, z)) {
        return getBloc(x, y, z)->getType() == BLOC_TYPES::PLEIN;
    } //else 
    return false;
}