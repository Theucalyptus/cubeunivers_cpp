#include  "ChunkBuilder.hpp"

#include "Monde.hpp"

using namespace std;

Chunk ChunkBuilder::GenererChunk(int seed, int coordX, int coordY) {
       
    FastNoise generateur;
    generateur.SetSeed(seed);

    
    Chunk temp_chunk = Chunk(coordX, coordY);

    // Génération map variation de COULEURS
    generateur.SetFrequency(0.005);
    float variation_couleur[CHUNK_SIZE][CHUNK_SIZE];
    for(int x=0;x<CHUNK_SIZE;++x) {
        for(int z=0;z<CHUNK_SIZE;++z) { 
            variation_couleur[x][z] = glm::clamp(generateur.GetSimplex(coordX*CHUNK_SIZE + x,coordY*CHUNK_SIZE + z), 0.0f, 1.0f);
        }
    }
   
    
    // Génération du terrain
    generateur.SetSeed(seed+1);
    generateur.SetFractalOctaves(4);
    generateur.SetFrequency(0.0005);

    int elevation_terrain[CHUNK_SIZE][CHUNK_SIZE];
    for(int x=0;x<CHUNK_SIZE;++x) {
        for(int z=0;z<CHUNK_SIZE;++z) {
            elevation_terrain[x][z] = getHauteur(generateur.GetSimplexFractal(coordX*CHUNK_SIZE + x,coordY*CHUNK_SIZE + z));
            
            for(int y=0;y<=elevation_terrain[x][z]-1;y++) { // Set du sol profond
                BlocPlein* b = new BlocPlein(PALETTE[PALETTE_INDEX::TERRE]);
                temp_chunk.setBlock(b, x, y, z);
            }
            for(int y=elevation_terrain[x][z]-1;y<=elevation_terrain[x][z];y++) { // Set de sol de surface                
                BlocPlein* b = new BlocPlein(PALETTE[PALETTE_INDEX::VERT_500]);
                temp_chunk.setBlock(b, x, y, z);
            }
        }

    }


    // Génération des nuages
    generateur.SetSeed(seed+2);
    generateur.SetFrequency(0.01);
    int base_hauteur =  CHUNK_HEIGHT*3/4;
    for(int x=0;x<CHUNK_SIZE;x++) {
        for(int z=0;z<CHUNK_SIZE;z++) { 
            float valeur = generateur.GetSimplex(coordX*CHUNK_SIZE +x, coordY*CHUNK_SIZE +z);
            if(valeur >= 0.5) {
                valeur = 2*valeur -1;
                for(int y=0;y<valeur*15;y++){
                    int correction = (1.0f-valeur) * 8;
                    BlocPlein* b = new BlocPlein(PALETTE[PALETTE_INDEX::BLANC]);
                    temp_chunk.setBlock(b, x, base_hauteur+correction+y, z);
                }
            }
        }
    } 


    // Placement des décorations - A Déplacer dans une fonction à l'échelle du monde
    Structure monArbre(STRUCTURES::ARBRE1);

    for(int x=3;x<CHUNK_SIZE-4;x++) {
        for(int z=3;z<CHUNK_SIZE-4;z++) { 
            float valeur = generateur.GetWhiteNoise(coordX*CHUNK_SIZE +x, coordY*CHUNK_SIZE +z);
            if(valeur >= 0.995) {
                for(int a=0;a<monArbre.tailleX;a++) {
                    for(int b=0;b<monArbre.tailleY;b++) { 
                        for(int c=0;c<monArbre.tailleZ;c++) { 
                            if(monArbre.m_blocs(a, b, c).type == BLOC_TYPES::PLEIN) {                         
                                BlocPlein* bloc = new BlocPlein(PALETTE[monArbre.m_blocs(a, b, c).couleur1]);
                                temp_chunk.setBlock(bloc, x+a-monArbre.offsetX,elevation_terrain[x][z]+1+b,z+c-monArbre.offsetZ);
                            }
                        }
                    }

                }
            }
        }
    } 



    return temp_chunk; 
}

int ChunkBuilder::getHauteur(float noiseLevel) {
    return  glm::clamp((int)((CHUNK_HEIGHT/4) * (noiseLevel + 1)), 1, CHUNK_HEIGHT*3/4); 
}
