#ifndef MONDE_H
#define MONDE_H


#include <iostream>
#include <cstdlib>
#include <glm/mat4x4.hpp>
#include <vector>

#include <thread>
#include <future>


#include "Bloc/Bloc.hpp"
#include "Chunk.hpp"
#include "ChunkBuilder.hpp"
#include "../Utils/Vecteurs.hpp"
#include "../Shaders/Shader.hpp"


#define RENDER_DISTANCE 20
#define LOAD_MARGIN 2
#define FOG_START 0.9f // 1 = RENDER_DISTANCE
#define FOG_END 1.0f // 1 = RENDER_DISTANCE

using namespace std;

const int CHUNKARRSIZE = RENDER_DISTANCE+LOAD_MARGIN*2;

class Monde {

    public:
        Monde();
        ~Monde();
 
        void Generer();
        void GenererMesh();
        //void CalculerLumiere();
        void Rendre(glm::mat4 projection, glm::mat4 view, float far); 


        // static Bloc m_voidBloc;
        // Bloc& getBloc(int coordX, int coordY, int coordZ);
        int getSeed();

    
    private:
        int m_seed;
        ChunkBuilder m_ChunkBuilder;
        Vecteur2D<Chunk> m_chunks;

        // Données pour le rendu 
        Shader main_shader;

};

#endif // MONDE_H
