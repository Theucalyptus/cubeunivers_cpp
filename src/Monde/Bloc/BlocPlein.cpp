#include  "BlocPlein.hpp"

using namespace std;

glm::vec3 BlocPlein::plein_vertices[6][4] = {    {   glm::vec3(-0.5,-0.5,-0.5),      glm::vec3(0.5,-0.5,-0.5),   // ARRIERE (ORDRE : SW, SE, SW, SE)
                                                glm::vec3(-0.5,0.5,-0.5),      glm::vec3(0.5,0.5,-0.5)},   // ARRIERE
                                            
                                            {   glm::vec3(0.5,-0.5,0.5),       glm::vec3(-0.5,-0.5,0.5 ),   // AVANT NE, NW, NE, NW
                                                glm::vec3(0.5,0.5,0.5),       glm::vec3(-0.5,0.5,0.5 )},   // AVANT
                                            
                                            {   glm::vec3(0.5,-0.5,-0.5),      glm::vec3(0.5,-0.5,0.5),   // DROITE SE, NE, SE NE 
                                                glm::vec3(0.5,0.5,-0.5),      glm::vec3(0.5,0.5,0.5)},   // DROITE

                                            {   glm::vec3(-0.5,-0.5,0.5),       glm::vec3(-0.5,-0.5,-0.5 ),   // Gauche NW, SW, NW, SW
                                                glm::vec3(-0.5,0.5,0.5),       glm::vec3(-0.5,0.5,-0.5 )},   // Gauche
                                            
                                            {   glm::vec3(0.5,-0.5,-0.5),      glm::vec3(-0.5,-0.5,-0.5),   // DESSOUS SE, SW, NE, NW
                                                glm::vec3(0.5,-0.5,0.5),      glm::vec3(-0.5,-0.5,0.5)},   // DESSOUS

                                            {   glm::vec3(-0.5,0.5,-0.5),        glm::vec3(0.5,0.5,-0.5), //DESSUS SE, SW, NE, NW
                                                glm::vec3(-0.5,0.5,0.5),        glm::vec3(0.5,0.5,0.5)}};  // DESSUS

glm::vec3 BlocPlein::plein_normales[6] = {   glm::vec3(0.0,  0.0,   -1.0), // Arrière
                                        glm::vec3(0.0,  0.0,    1.0), // Avant
                                        glm::vec3(-1.0, 0.0,    0.0), // Droite
                                        glm::vec3(1.0,  0.0,    0.0), // Gauche
                                        glm::vec3(0.0,  -1.0,   0.0), // Dessous
                                        glm::vec3(0.0,  1.0,    0.0), // Dessus

};


BlocPlein::BlocPlein(Couleur NE, Couleur NW, Couleur SE, Couleur SW) : Bloc(BLOC_TYPES::PLEIN) {
    // Constructeur
    NE = NE;
    NW = NW;
    SW = SW;
    SE = SE;
}

BlocPlein::BlocPlein(Couleur couleur) : Bloc(BLOC_TYPES::PLEIN) {
    // Constructeur
    NE = SE = SW = NW = couleur;
}


BlocPlein::~BlocPlein() {
    // Deconstructeur
}


// Getters

vector<glm::vec3> BlocPlein::getVerticesFace(DIRECTION face) {
    vector<glm::vec3> temp;
    for(int i=0;i<4;i++) {
        temp.push_back(plein_vertices[as_integer(face)][i]);
    }

    return temp;
}

glm::vec3 BlocPlein::getNormalesFace(DIRECTION face) {
    return plein_normales[as_integer(face)];
}


void BlocPlein::setCouleurDirection(DIRECTION direction, Couleur newCouleur) {
    switch (direction)
    {
        case DIRECTION::NORD_EST:
            NE = newCouleur;
            break;
        case DIRECTION::NORD_OUST:
            NW = newCouleur;
            break;
        case DIRECTION::SUD_EST:
            SE = newCouleur;
            break;
        case DIRECTION::SUD_OUEST:
            SW = newCouleur;
            break;
        default:
            break;
    }
}
void BlocPlein::setCouleurAll(Couleur newCouleur) {
    NE = SE = NW = SW = newCouleur;
}

Couleur BlocPlein::getCouleurForMesh(DIRECTION direction, int index) {
    
    Couleur output;
    switch (direction)
    {
        case DIRECTION::ARRIERE:
            if(index == 0 || index == 2) {
                output = SW;
            }
            else {
                output = SE;
            }
            break;
        case DIRECTION::AVANT:
            if(index == 0 || index == 2) {
                output = NE;
            }
            else {
                output = NW;
            }
            break;
        case DIRECTION::DROITE:
            if(index == 0 || index == 2) {
                output = SE;
            }
            else {
                output = NE;
            }
            break;
            break;

        case DIRECTION::GAUCHE:
            if(index == 0 || index == 2) {
                output = NW;
            }
            else {
                output = SW;
            }
            break;
        case DIRECTION::HAUT:
        case DIRECTION::BAS:
            if(index == 0) {
                output = SE;
            }
            else if(index == 1) {
                output = SW;
            }
            else if (index == 2) {
                output = NE;
            } else {
                output = NW;
            }
        default:
            output = PALETTE[PALETTE_INDEX::NOIR];
            break;
    }

    return output;
}

BlocPlein* BlocPlein::fromBlocPtr(Bloc* blocPlein) {
    return dynamic_cast<BlocPlein*>(blocPlein);
}