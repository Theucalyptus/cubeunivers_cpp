#ifndef BLOC_H
#define BLOC_H

#include <memory>
#include <iostream>

#include "../../Autres/Enumerations.hpp"


class Bloc {

    public:
        Bloc();
        Bloc(BLOC_TYPES type);      
        virtual ~Bloc();

        BLOC_TYPES getType();

    private:
        BLOC_TYPES m_type;

};



#endif // BLOC_H
