#include  "Bloc.hpp"

using namespace std;


Bloc::Bloc() : m_type(BLOC_TYPES::AIR) {
    // Constucteur par défaut
}

Bloc::Bloc(BLOC_TYPES type) : m_type(type) {
    // Constructeur type spécifié
}

Bloc::~Bloc() {
}

// Getter du type
BLOC_TYPES Bloc::getType() {
    return m_type;
}
