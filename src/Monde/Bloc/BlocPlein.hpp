#ifndef BLOCPLEIN_H
#define BLOCPLEIN_H

#include <iostream>
#include <glm/glm.hpp>
#include <vector>

#include "Bloc.hpp"
#include "../../Autres/Types.hpp"

class BlocPlein: public Bloc {

    public:
        BlocPlein(Couleur couleur);
        BlocPlein(Couleur NE, Couleur SE, Couleur NW, Couleur SW);
        virtual ~BlocPlein();


        static glm::vec3 plein_vertices[6][4];
        static glm::vec3 plein_normales[6];

        void setCouleurDirection(DIRECTION direction, Couleur newCouleur);
        void setCouleurAll(Couleur newCouleur);
        Couleur getCouleurForMesh(DIRECTION face, int index);

        std::vector<glm::vec3> getVerticesFace(DIRECTION face);
        glm::vec3 getNormalesFace(DIRECTION face);

        static BlocPlein* fromBlocPtr(Bloc* blocPlein);

    private:
        Couleur NE, NW, SE, SW;
    
};

#endif // BLOCPLEIN_H
