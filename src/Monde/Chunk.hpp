#ifndef CHUNK_H
#define CHUNK_H

#include <GL/glew.h>
#include <glm/mat4x4.hpp>
#include <glm/vec3.hpp>
#include <glm/ext/matrix_transform.hpp>
#include <vector>
#include <iostream>

#include "Bloc/Bloc.hpp"
#include "Bloc/BlocPlein.hpp"
#include "../Shaders/Shader.hpp"
#include "../Utils/Vecteurs.hpp"

#define CHUNK_SIZE 20
#define CHUNK_HEIGHT 160

class Monde;

class Chunk {

    public:
        Chunk();
        Chunk(int coordX, int coordY);
        ~Chunk();

        void GenererMesh();
        void GenererVBO(std::vector<GLfloat> &data);
        void GenererEBO(std::vector<GLuint> &data);
        void GenererVAO();
        void Rendre();
   
        glm::mat4 getModel();


        Bloc* getBloc(int coordX, int coordY, int coordZ);
        void setBlock(Bloc* bloc, int x, int y, int z);
        int getCoordX();
        int getCoordY();

    private:
        //Données format in-game
        Vecteur3D<Bloc*> m_blocs;
        int m_nbrObjet;
        glm::mat4 m_model;
        int m_coordX, m_coordY;

        int x_offset;
        int z_offset;

        bool isValidChunkCoord(int bX, int bY, int bZ);
        bool isSolidBlock(int x, int y, int z);
        
        void _ajouterFace2Mesh(DIRECTION face, std::vector<GLfloat> &vbo_data, std::vector<GLuint> &ebo_data, glm::vec3 coord_bloc); //ajoute tout les infos pour afficher un bloc aux mesh
        //glm::vec3* _lisserCouleur(glm::vec3 coord_bloc); // calcul couleur des coins d'un bloc
        //float* _lisserLumiere(glm::vec3 coord_block);

        // Valeur pour le rendu
        GLuint m_vboID;
        GLuint m_eboID;
        GLuint m_vaoID;


        
};

typedef std::unique_ptr<Chunk> PChunk;


#endif // CHUNK_H
