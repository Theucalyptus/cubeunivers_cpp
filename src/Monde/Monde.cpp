#include  "Monde.hpp"

using namespace std;


Monde::Monde() : main_shader("Shaders/basic.vert", "Shaders/basic.frag"), m_chunks(CHUNKARRSIZE, CHUNKARRSIZE) {
    // Constructeur
    m_seed = rand() % 9999;
    main_shader.Charger();    

}

Monde::~Monde() {    
    // Deconstructeur

}

void Monde::Generer() {
        
    std::cout << "Monde::Générer démarrée" << std::endl;

    std::vector<std::future<Chunk>> myFutures;

    for(int x=0;x<RENDER_DISTANCE+LOAD_MARGIN*2;x++){
        for(int y=0;y<RENDER_DISTANCE+LOAD_MARGIN*2;y++){
                std::future<Chunk> fut = std::async(&ChunkBuilder::GenererChunk, m_seed, x-RENDER_DISTANCE/2-LOAD_MARGIN, y-RENDER_DISTANCE/2-LOAD_MARGIN); 
                myFutures.push_back(std::move(fut));
        }
    }

    for (int i=0;i<myFutures.size();i++) {
        auto fut = std::move(myFutures[i]);
        if (fut.valid()) {
            Chunk chunk = fut.get();
            int coX = chunk.getCoordX()+RENDER_DISTANCE/2+LOAD_MARGIN;
            int coY = chunk.getCoordY()+RENDER_DISTANCE/2+LOAD_MARGIN;
            m_chunks(coX, coY) = chunk;
        }
        else {
            std::cout << "Monde::Générer future get() invalide !" << std::endl;
        }

    }

    std::cout << "Monde::Générer terminé !" << std::endl;

}

// void Monde::CalculerLumiere() {
//     for(int x=LOAD_MARGIN;x<RENDER_DISTANCE+LOAD_MARGIN;++x){
//         for(int y=LOAD_MARGIN;y<RENDER_DISTANCE+LOAD_MARGIN;++y){
//             cout << "Calcul des lumières de " << x <<  " " << y << endl;
//             m_chunks(x, y)->CalculerLumiere();
//         }
//     }
// }

void Monde::GenererMesh() {

    std::vector<std::thread> workers;

    for(int x=LOAD_MARGIN;x<RENDER_DISTANCE+LOAD_MARGIN;++x){
        for(int y=LOAD_MARGIN;y<RENDER_DISTANCE+LOAD_MARGIN;++y){
            m_chunks(x, y).GenererMesh();
        }
    }

    std::cout << "Monde::GenererMesh terminé" << endl;

}

void Monde::Rendre(glm::mat4 projection, glm::mat4 view, float far) {  
    
    glUseProgram(main_shader.getProgramID());
    
    main_shader.UniformMat4((char*)"view", view);
    main_shader.UniformMat4((char*)"projection", projection);
    main_shader.UniformFloat((char*)"CHUNK_HEIGHT", CHUNK_HEIGHT);
    
    // Uniform pour le brouillard dans fragment Shader
    main_shader.UniformFloat((char*)"brouillard.far", far);
    main_shader.UniformFloat((char*)"brouillard.start", FOG_START);
    main_shader.UniformFloat((char*)"brouillard.end", FOG_END);
    main_shader.UniformFloat((char*)"brouillard.lvl_max", 1.0);
    main_shader.UniformFloat((char*)"brouillard.proche", 0.1);
    main_shader.UniformVec3((char*)"brouillard.couleur", glm::vec3(0.0,0.0,1.0));

    // Uniform pour le soleil
    main_shader.UniformVec3((char*)"soleil.couleur", glm::vec3(1.0,1.0,1.0));
    main_shader.UniformVec3((char*)"soleil.direction", glm::normalize(glm::vec3(0.5, 1.0,0.25)));

    for(int x=LOAD_MARGIN;x<RENDER_DISTANCE+LOAD_MARGIN;++x){
        for(int y=LOAD_MARGIN;y<RENDER_DISTANCE+LOAD_MARGIN;++y){
            main_shader.UniformMat4((char*)"model", m_chunks(x, y).getModel());
            m_chunks(x, y).Rendre();
        }
    }
    glUseProgram(0);
}

// Bloc& Monde::getBloc(int coordX, int coordY, int coordZ) {

//     coordX += CHUNK_SIZE * ((RENDER_DISTANCE/2)+LOAD_MARGIN);
//     coordZ += CHUNK_SIZE * ((RENDER_DISTANCE/2)+LOAD_MARGIN);
    
//     int limite = CHUNK_SIZE*(RENDER_DISTANCE+LOAD_MARGIN*2);

//     if(coordX < 0 || coordZ < 0 || coordY < 0 || coordX >= limite || coordZ >= limite || coordY >= CHUNK_HEIGHT) {
//         return &m_voidBloc;
//     }
//     else {
//         return m_chunks(coordX/CHUNK_SIZE, coordZ/CHUNK_SIZE).getBloc(coordX%CHUNK_SIZE, coordY, coordZ%CHUNK_SIZE);
//     }


// }

int Monde::getSeed() {
    return m_seed;
}
