#ifndef ENUMERATIONS_H
#define ENUMERATIONS_H

#include <glm/vec3.hpp>
#include "Types.hpp"

// Template pour récupérer les valeurs prédéfinies correspondantes:            
template <typename T>
auto as_integer(T const value) -> typename std::underlying_type<T>::type
{
    return static_cast<typename std::underlying_type<T>::type>(value);
}

// Couleurs: 
struct PALETTE_INDEX {  
    const static unsigned short NOIR =0;
    const static unsigned short GRIS =1; 
    const static unsigned short BLANC = 2; 
    const static unsigned short ROUGE_300 =3; 
    const static unsigned short ROUGE_500 =4;
    const static unsigned short ROUGE_700 =5; 
    const static unsigned short VERT_300 =6;
    const static unsigned short VERT_500=7; 
    const static unsigned short VERT_700=8; 
    const static unsigned short BLEU_300=9; 
    const static unsigned short BLEU_500=10; 
    const static unsigned short BLEU_700=11;
    const static unsigned short JAUNE_300=12; 
    const static unsigned short JAUNE_500=13; 
    const static unsigned short JAUNE_700=14;
    const static unsigned short TERRE=15;
};

static Couleur PALETTE[] = {     Couleur(0.0,0.0,0.0),     //NOIR
                                    Couleur(0.33,0.33,0.33),  //GRIS
                                    Couleur(1.0,1.0,1.0),     //BLANC
                                    Couleur(0.6,0.0,0.0), //ROUGE_300
                                    Couleur(1.0,0.0,0.0), //ROUGE_500
                                    Couleur(1.0,0.4,0.4), //ROUGE_700
                              
                                    
                                    Couleur(0.0,0.6,0.0), //VERT_300
                                    Couleur(0.0,1.0,0.0), //VERT_500
                                    Couleur(0.4,1.0,0.4), //VERT_700
                                    
                                    Couleur(0.0,0.0,0.6), //BLEU_300
                                    Couleur(0.0,0.0,1.0), //BLEU_500
                                    Couleur(0.4,0.4,1.0), //BLEU_700


                                    Couleur(0.4,0.4,0.0), //JAUNE_300
                                    Couleur(1.0,1.0,0.0), //JAUNE_500
                                    Couleur(0.4,0.4,0.4), //JAUNE_700
                                    

                                    // Couleurs Spéciales
                                    Couleur(0.98,0.73,0.38) // Terre
                                    };


// Blocs
enum class BLOC_TYPES : unsigned short {AIR, PLEIN};
enum class FORMAT_VERTICES : bool {BRUTE, COMPOSEE};

// STRUCTURES
enum class STRUCTURES : unsigned short {ARBRE1, ARBRE2};
static char* fichiers_structure[] = {(char*)"Monde/Structures/arbre1.json", (char*)"Monde/Structures/arbre2.json"};

// MISC
enum class DIRECTION : int {AVANT,ARRIERE,DROITE,GAUCHE, BAS,HAUT, NORD_EST, SUD_EST, SUD_OUEST, NORD_OUST};



#define VECTEUR_VERTICAL Couleur(0.0,1.0,0.0)



#endif // ENUMERATIONS_H