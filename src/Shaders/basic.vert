#version 400

layout (location=0) in vec3 position;
layout (location=1) in vec3 couleur;
layout (location=2) in vec3 normale;
layout (location=3) in float niveauLumiere;


struct Soleil
{
    vec3 direction;
    vec3 couleur;
};
uniform Soleil soleil;

uniform mat4 projection;
uniform mat4 view;
uniform mat4 model;
uniform float CHUNK_HEIGHT;

out vec3 vert_couleur;

void main() {
    gl_Position = projection * view * model * vec4(position, 1.0);
    float puissanceSoleil = max(dot(normale, soleil.direction), 0.0);
    float puissanceAmbiante = 0.3;
    float eclairage = min(puissanceSoleil*niveauLumiere + puissanceAmbiante, 1.0);
    vert_couleur = couleur * eclairage;
}