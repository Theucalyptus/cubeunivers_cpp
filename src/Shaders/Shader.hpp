#ifndef SHADER_H
#define SHADER_H

#include <GL/glew.h>
#include <string>

#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>
#include <glm/gtc/type_ptr.hpp>


class Shader {

    public:
        Shader();
        Shader(std::string vertexShader, std::string fragmentShader);
        Shader(std::string vertexShader, std::string fragmentShader, std::string geometryShader);
        virtual ~Shader();
        
        // Fonction de Chargement
        bool CompilerShader(GLuint &shaderID, GLenum typeShader, std::string cheminFichierSource);
        bool Charger();


        // Helpers pour les uniforms
        void UniformVec3(char cible[], glm::vec3 vecteur);
        void UniformMat4(char cible[], glm::mat4 matrice);
        void UniformFloat(char cible[], GLfloat valeur);

        // Getters
        GLuint getProgramID();


    private:
        GLuint m_vertexID;
        GLuint m_geometryID;
        GLuint m_fragmentID;
        GLuint m_programID;

        std::string m_vertex_source;
        std::string m_geometry_source;
        std::string m_fragment_source;

        bool m_chargee;
        short m_mode;





};

#endif // SHADER_H
