#version 400
out vec4 frag_couleur;
in vec3 vert_couleur;

struct Brouillard {
    float proche; 
    float far; 
    float start;
    float end;
    float lvl_max;
    vec3 couleur;
};
uniform Brouillard brouillard;

struct Lumiere {
    vec3 position;
    vec3 couleur;
    float puissance;
};
uniform Lumiere lumieres;

float LineariserProfondeur(float profondeur)  {
    float z = profondeur * 2.0 - 1.0;
    return ((2.0 * brouillard.proche * brouillard.far) / (brouillard.far + brouillard.proche - z * (brouillard.far - brouillard.proche))) / brouillard.far;	
}

float niveauBrouillard(float profondeur_linearisee) {
    float coef = 1/(brouillard.end-brouillard.start);    
    return clamp((profondeur_linearisee-brouillard.start)*coef, 0.0, brouillard.lvl_max);
}

void main() {

    float puissanceBrouillard = niveauBrouillard(LineariserProfondeur(gl_FragCoord.z));
    vec3 couleur = vert_couleur; // * vert_niveauLumiere;
    frag_couleur = vec4( mix(couleur, brouillard.couleur, puissanceBrouillard), 1.0);
}