#include  "Shader.hpp"

#include <iostream>
#include <fstream>
#include <string>

using namespace std;

Shader::Shader() : m_mode(0), m_chargee(false), m_vertexID(0), m_fragmentID(0), m_geometryID(0), m_programID(0), m_vertex_source(), m_fragment_source(), m_geometry_source() {
    // Constructeur par défaut
}

Shader::Shader(string vertexShader, string fragmentShader) : m_mode(0), m_chargee(false), m_vertexID(0), m_fragmentID(0), m_programID(0), m_vertex_source(vertexShader), m_fragment_source(fragmentShader) {
}

Shader::Shader(string vertexShader, string fragmentShader, string geomtryShader) : m_mode(1), m_chargee(false), m_vertexID(0), m_fragmentID(0), m_programID(0), m_vertex_source(vertexShader), m_fragment_source(fragmentShader), m_geometry_source(geomtryShader) {

}

Shader::~Shader() { //Deconstructeur propre

    glDeleteProgram(m_programID);
    glDeleteShader(m_vertexID);
    glDeleteShader(m_fragmentID);
    if(m_mode == 1) {
        glDeleteShader(m_geometryID); 
    }
    
}

bool Shader::CompilerShader(GLuint &shaderID, GLenum typeShader, string cheminFichierSource) {
    
    shaderID = glCreateShader(typeShader);
    if (shaderID == 0) {
        // Si l'objet shader n'est pas été crée:
        cout << "Erreur à la création du shader : " << typeShader << " invalide." << endl;
        return false;
    }


    ifstream fichier(cheminFichierSource.c_str());
    if(!fichier) {
        // Si le fichier source n'est pas ouvrable:
        cout << "Erreur à l'ouverture du fichier shader source " << cheminFichierSource << "." << endl;
        glDeleteShader(shaderID);
        return false;
    }


    string codeSource;
    string ligne;
    while(getline(fichier, ligne)) {
        codeSource += ligne + '\n';
    }

    const GLchar* sources_final = codeSource.c_str();
    glShaderSource(shaderID, 1, &sources_final, 0);
    glCompileShader(shaderID);
    
    GLint succes(0);
    glGetShaderiv(shaderID, GL_COMPILE_STATUS, &succes);
        
    if(succes != GL_TRUE) {
        
        
        //Si le shader n'a pas compilé:
        GLint tailleErreur(0);
        glGetShaderiv(shaderID, GL_INFO_LOG_LENGTH, &tailleErreur);
        char *erreur = new char[tailleErreur + 1];
        glGetShaderInfoLog(shaderID, tailleErreur, &tailleErreur, erreur);
        erreur[tailleErreur] = '\0';
        
        cout << "Erreur à la compilation du shader " << cheminFichierSource << ". || CAUSE: " << erreur << endl;
        
        delete[] erreur;
        glDeleteShader(shaderID);
        return false;
    } 
    else
        return true; 
}

bool Shader::Charger() {
    
    if(glIsShader(m_vertexID)) {
        glDeleteShader(m_vertexID);
    }
    if(glIsShader(m_geometryID)) {
        glDeleteShader(m_geometryID);
    }
    if(glIsShader(m_fragmentID)) {
        glDeleteShader(m_fragmentID);
    }
    if(glIsProgram(m_programID)) {
        glDeleteProgram(m_programID);
    }

    if(!CompilerShader(m_vertexID, GL_VERTEX_SHADER, m_vertex_source)) { return false;}
    if(!CompilerShader(m_fragmentID, GL_FRAGMENT_SHADER, m_fragment_source)) {return false;}
    if(m_mode == 1) {
        if(!CompilerShader(m_geometryID, GL_GEOMETRY_SHADER, m_geometry_source)) {return false;}
    }

    m_programID = glCreateProgram();
    if (m_programID == 0) {
        cout << "Erreur à la création du programme." << endl;
        return false;
    }
    
    glAttachShader(m_programID, m_vertexID);
    glAttachShader(m_programID, m_fragmentID);
    if(m_mode == 1) {
            glAttachShader(m_programID, m_geometryID);
        }

    // LINKAGE DU Shader
    GLint succes(0);
    glLinkProgram(m_programID);

    glDetachShader(m_programID, m_vertexID);
    glDetachShader(m_programID, m_fragmentID);
    if(m_mode == 1) {
        glDetachShader(m_programID, m_geometryID);
    }

     // Vérification si le linkage c'est effectué avec succés.
    glGetProgramiv(m_programID, GL_LINK_STATUS, &succes);
    if(succes != GL_TRUE) {
        GLint tailleErreur(0);
        glGetProgramiv(m_programID, GL_INFO_LOG_LENGTH, &tailleErreur);
        char *erreur = new char[tailleErreur + 1];
        glGetProgramInfoLog(m_programID, tailleErreur, &tailleErreur, erreur);
        erreur[tailleErreur] = '\0';
        
        cout << "Erreur au linkage du programme. || CAUSE: " << erreur << endl;

        glDeleteProgram(m_programID);
        glDeleteShader(m_vertexID);
        glDeleteShader(m_fragmentID);
        if(m_mode == 1) {
            glDeleteShader(m_geometryID); 
        }

        return false;

    }

    m_chargee = true;
    return true;
}

void Shader::UniformVec3(char cible[], glm::vec3 vecteur) {
    glUniform3fv(glGetUniformLocation(m_programID, cible), 1, value_ptr(vecteur));
}

void Shader::UniformMat4(char cible[], glm::mat4 matrice) {
    glUniformMatrix4fv(glGetUniformLocation(m_programID, cible), 1, GL_FALSE, glm::value_ptr(matrice));
}

void Shader::UniformFloat(char cible[], GLfloat valeur) {
    glUniform1f(glGetUniformLocation(m_programID, cible), valeur);
}

GLuint Shader::getProgramID() {
    return m_programID;
}