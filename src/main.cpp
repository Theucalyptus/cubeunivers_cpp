#include <iostream>
#include <string>
#include <SDL2/SDL.h>
#include <GL/glew.h>
#include <glm/mat4x4.hpp>
#include <glm/ext/matrix_transform.hpp>
#include <math.h>

#include "Utils/SDLManager.hpp"
#include "Utils/Input.hpp"
#include "Camera.hpp"
#include "Joueur.hpp"
#include "Monde/Monde.hpp"


using namespace std;

int main(int, char**) {

    if(!InitialiserSDL()) {
        return -1;
    }

    SDL_Window* fenetre = CreerFenetre( (char*) "Cube:Univers");
    if(!CreerContextGL(fenetre)) {
        return -1;
    }

    if(!InitialiserGLEW()) {
        return -1;
    }

    Input input;
    Joueur monJoueur;
    Camera maCamera(monJoueur);

    
    float far = (RENDER_DISTANCE/2)*CHUNK_SIZE;
    glm::mat4 projection = glm::perspective(70.0, (double) RES_X/RES_Y, 0.1, (double)far);

    unsigned int debut = SDL_GetTicks();
    
    Monde monde = Monde();
    monde.Generer();
    //monde.CalculerLumiere();
    monde.GenererMesh();

    while (!input.Terminer()) {
        
        input.UpdateInput(monJoueur, maCamera);
        maCamera.Cibler();

        glClearColor(0.0,0.0,1.0,1.0);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        monde.Rendre(projection, maCamera.getViewMatrice(), far);

        SDL_GL_SwapWindow(fenetre);
    }

    cout << "EXIT" << endl;

    QuitterSDL(fenetre);
    return 0;
}
