#include <iostream>

#include <vector>

using namespace std;

template <typename T>
class Vecteur1D {
public:
    Vecteur1D(size_t tailleX) : tailleX(tailleX), donnees(tailleX){
    }

    T & operator()(size_t x) {
        return donnees[x];
    }

    T const & operator()(size_t x) const {
        return donnees[x];
    }

    private:
        size_t tailleX;
        std::vector<T> donnees;
};

class A {
    public:
        A() {
            cout << "Constructeur A" << endl;
        };
        void dispA() {
            cout << "A" << endl;
        }

        virtual ~A() {};
};

class B: public A {
    public:
        B() {
            cout << "B Constructeur" << endl;
        }
        void dispB() {
            cout << "B" << endl;
        }
};

int main(int, char**) {

    Vecteur1D<A*> arr = Vecteur1D<A*>(1);
    arr(0) = new B();

    A* fakeA = arr(0);
    (*fakeA).dispA();
    auto bPtr = dynamic_cast<B*>(fakeA);
    if(bPtr) {
        bPtr->dispB();
    } else {
        cout << "fail" << endl;
    }

    return 0;
}