#include <iostream>

#include <vector>

using namespace std;

template <typename T>
class Vecteur1D {
public:
    Vecteur1D(size_t tailleX) : tailleX(tailleX), donnees(tailleX){
    }

    T & operator()(size_t x) {
        return donnees[x];
    }

    T const & operator()(size_t x) const {
        return donnees[x];
    }

    private:
        size_t tailleX;
        std::vector<T> donnees;
};


class Chunk{

    public:
        Chunk() {
            cout << "Chunk Allocator Called" << endl;
        }
        Chunk(int xVal) : x(xVal) {};
        ~Chunk() {
            cout << "Chunk destructeur called" << endl;
        };

        void setX(int newX) {
            x=newX;
        }

        void print() {
            cout << "Chunk " << x << endl;
        }

    private:
        int x;
};

class ChunkBuild {

    public:
        static Chunk build(int x) {
            Chunk tempChunk = Chunk(x);
            return tempChunk;
        }

};

class Monde {

    public:
        Monde(int size) : size(size), arr(size) {}
        void Generer() {
            for(int i=0;i<size;i++) {
                cout << "Set de arr(" << i << ")" << endl;
                arr(i) = ChunkBuild::build(i);
            }
        }

        void Afficher() {
            cout << "Monde" << endl;
            for(int i=0;i<size;i++) {
                arr(i).print();
            }
            cout << "Fin Monde" << endl << endl;
        }
    private:
        int size;
        Vecteur1D<Chunk> arr;
};




int main(int, char**) {

    Monde monde = Monde(5);
    monde.Afficher();
    monde.Generer();
    monde.Afficher();

    return 0;
}