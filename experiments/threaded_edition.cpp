#include <iostream>
#include <thread>

using namespace std;

class A {

    public:
        A(int x=2) : mx(x) {};
        void print() {
            cout << mx << endl;
        }

        void setX(int newX) {
            mx = newX;
        }
    private: 
        int mx;
};

int main(int argc, char** argv) {

    A monA = A(56);
    monA.print();
    std::thread monThread = std::thread(&A::setX, &monA, 99);
    monThread.join();
    monA.print();

    return 0;
}